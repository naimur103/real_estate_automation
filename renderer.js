// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const scraper = require("./scraper");
const debug = require("debug")("scraper");
const json2csv = require("json2csv").parse;
const home = require("os").homedir();
const fs = require("fs");
let filename;
const unmatchedFields = [
	"INSTRUMENT",
	"DOCUMENT TYPE",
	"RECORDED DATE",
	"MUNICIPALITY",
	"BOOK",
	"PAGE",
	"BLOCK",
	"LOT",
	"SECONDARY NAME"
];
const matchedFields = [
	...unmatchedFields,
	"PROP LOC",
	"DISTRICT",
	"OWNER",
	"STREET",
	"CITY STREET",
	"SALE DATE",
	"PRICE",
	"NU#",
	"URL LINK",
	"EQUITY",
	"PURCHASE DATE",
	"STATUS",
	"ESTIMATED VALUE"
];
function writeCsv(data, fields) {
	const newLine = "\r\n";
	let dataToAdd = [];
	dataToAdd.push(data);
	const merged = [].concat.apply([], dataToAdd);
	filename = `${home}/Desktop/${
		fields.length > 13 ? "matched" : "unmatched"
	}.csv`;
	//write the headers and newline
	debug("New file, just writing headers");
	const newCsv = json2csv(merged, { fields }) + newLine;
	fs.writeFile(`${filename}`, newCsv, function(err, stat) {
		if (err) throw err;
		debug("file saved to ", filename);
	});
}

function ready() {
	let count = 0;
	$(document).ready(function() {
		$("select").formSelect();
		$("form").show();
		$("#message, #progress, #time, #msg, #back").hide();
		// on click back
		$("#back").click(reset);
		// get form values
		$("form").on("submit", function(e) {
			e.preventDefault();
			// show messages, loader and time
			// hide form
			$("form").hide();
			$("#message, #progress, #time").show();
			// get input data
			let inputData = {};
			inputData.username = $("#username").val();
			inputData.password = $("#password").val();
			inputData.documentType = [$("#documentType").val()];
			inputData.displayRecord = $("#displayRecord").val();
			inputData.dateRange = $("#dateRange").val();
			// Change the status of bot
			$(".status").text("Bot running");
			const timer = setInterval(function() {
				count = count + 1;
				$(".showTime").text(secondsToHms(count));
			}, 1000);
			// pass the data to scraper
			scraper(inputData).then(data => {
				if (data.error) {
					console.log(`Error: ${data.error}`);
					// TODO: some thing will happen
					return onFinish(timer, data.error);
				} else {
					const matched = data.filter(data => data.matched);
					const not_matched = data.filter(data => !data.matched);
					if (matched.length) {
						writeCsv(matched, matchedFields);
					}
					if (not_matched.length) {
						writeCsv(not_matched, unmatchedFields);
					}
					$(".status").text("Scraping Complete");
					// clear time out
					clearTimeout(timer);
					$("#progress").hide();
					$("#msg, #back").show();
					$(".showTime").text(secondsToHms(count));
					$("#msg").html(`<p>File saved to Desktop folder.</p>`);
					return true;
				}
			});
		});
	});

	function onFinish(timer, message) {
		clearTimeout(timer);
		$(".status").text("Scraping Stopped");
		$("#progress").hide();
		$("#msg, #back").show();
		$(".showTime").text(secondsToHms(count));
		$("#msg").html(message);
	}

	function reset() {
		location.reload();
	}

	function secondsToHms(d) {
		d = Number(d);
		const h = Math.floor(d / 3600);
		const m = Math.floor((d % 3600) / 60);
		const s = Math.floor((d % 3600) % 60);
		const hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours ") : "";
		const mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes ") : "";
		const sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
		return hDisplay + mDisplay + sDisplay;
	}
}

ready();
