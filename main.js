const electron = require("electron");
const { app, BrowserWindow } = electron;
const isDev = require("electron-is-dev");
require("electron-debug")();
if (isDev) {
	require("electron-reload")(__dirname, {
		electron: require("${__dirname}/../../node_modules/electron")
	});
}

let mainWindow;

function createWindow() {
	mainWindow = new BrowserWindow({
		width: 400,
		height: 400,
		show: false,
		backgroundColor: "#0984e3"
	});
	mainWindow.setMenu(null);
	mainWindow.loadFile(`index.html`);
	mainWindow.webContents.on("did-finish-load", function() {
		mainWindow.show();
	});
	mainWindow.on("closed", () => (mainWindow = null));
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") {
		app.quit();
	}
});

app.on("activate", () => {
	if (mainWindow === null) {
		createWindow();
	}
});
