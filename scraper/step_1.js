const puppeteer = require("puppeteer");
const strftime = require("strftime");
const debug = require("debug")(`scraper:`);
const Scraper = require("./common/Scraper");

class CustomScraper extends Scraper {
	constructor(props) {
		super(props);
		this.data = {
			extracted: {}
		};
		if (props.data) {
			this.data = Object.assign({}, this.data, props.data);
		}
	}
	async waitForFrame(name, selector) {
		const bodyFrame = await this.page
			.frames()
			.find(f => f.name() === "bodyframe");
		const frame = await this.page.frames().find(f => f.name() === name);
		await frame.waitForSelector(selector);
		return frame;
	}
	async setHiddenInput({ name, selector, value }) {
		try {
			const frame = await this.waitForFrame(name, selector);
			await frame.evaluate(
				({ selector, value }) =>
					(document.querySelector(selector).value = value),
				{ selector, value }
			);
		} catch (error) {
			debug(error);
		}
	}
	async removeTreeOptions({ name, selector }) {
		try {
			const frame = await this.waitForFrame(name, selector);
			await frame.evaluate(
				({ selector }) => document.querySelector(selector).click(),
				{ selector }
			);
		} catch (error) {
			debug(error);
		}
	}
	async setTreeOptions({ name, selector, documentType }) {
		try {
			const frame = await this.waitForFrame(name, selector);
			const types = documentType.map(type => type.toLowerCase());
			await frame.evaluate(
				({ selector, types }) => {
					document.querySelector("#_easyui_tree_13 .tree-hit").click();
					const doms = [...document.querySelectorAll(selector)];
					const targetDoms = doms.filter(function(dom) {
						return types.indexOf(dom.textContent.toLowerCase()) != -1;
					});
					targetDoms.map(e => e.click());
				},
				{ selector, types }
			);
		} catch (error) {
			debug(error);
		}
	}
	async clickFrame({ name, selector }) {
		try {
			const frame = await this.waitForFrame(name, selector);
			await frame.evaluate(
				({ selector }) => document.querySelector(selector).click(),

				{ selector }
			);
		} catch (error) {
			debug(error);
		}
	}
	async extractOtherName({ name, selector }) {
		await this.page.waitForSelector('[name="bodyframe"]');
		const bodyFrame = await this.page
			.frames()
			.find(f => f.name() === "bodyframe");
		await this.page.waitFor(3000);
		const errorFrame = this.page
			.frames()
			.find(f => f.name() === "dynSearchFrame");
		const errorMessage = await errorFrame.evaluate(() =>
			document.querySelector("#displayMsg").innerHTML.trim()
		);
		if (errorMessage.length > 0) {
			this.data.extracted.error = errorMessage;
		} else {
			await bodyFrame.waitForSelector('[name="resultFrame"]', {
				visible: true
			});
			const frame = await this.page.frames().find(f => f.name() === name);
			const result = await frame.evaluate(selector => {
				let td = [...document.querySelectorAll(selector)];
				td.shift();
				const data = td.map(e => {
					let result = {};
					result.name = e.querySelector("span").textContent;
					return result;
				});
				return data;
			}, selector);
			this.data.extracted.names = result;
		}
		return this;
	}

	async extractInstrument({ name, selector }) {
		await this.page.waitForSelector('[name="bodyframe"]');
		const bodyFrame = await this.page
			.frames()
			.find(f => f.name() === "bodyframe");
		await bodyFrame.waitForSelector('[name="resultFrame"]', { visible: true });
		const frame = await this.page.frames().find(f => f.name() === name);
		const result = await frame.evaluate(selector => {
			let td = [...document.querySelectorAll(selector)];
			td.shift();
			const data = td.map(e => {
				let result = {};
				result.instrument = e.textContent.trim();
				return result;
			});
			return data;
		}, selector);
		this.data.extracted.instruments = result;
		return this;
	}

	async extractMunicipality({ name, selector }) {
		await this.page.waitForSelector('[name="bodyframe"]');
		const bodyFrame = await this.page
			.frames()
			.find(f => f.name() === "bodyframe");
		await bodyFrame.waitForSelector('[name="resultFrame"]', { visible: true });
		const frame = await this.page.frames().find(f => f.name() === name);
		const result = await frame.evaluate(selector => {
			let td = [...document.querySelectorAll(selector)];
			td.shift();
			const data = td.map(e => {
				let result = {};
				result.municipality = e.textContent.trim();
				return result;
			});
			return data;
		}, selector);
		this.data.extracted.municipalities = result;
		return this;
	}

	async extractDocumentType({ name, selector }) {
		await this.page.waitForSelector('[name="bodyframe"]');
		const bodyFrame = await this.page
			.frames()
			.find(f => f.name() === "bodyframe");
		await bodyFrame.waitForSelector('[name="resultFrame"]', { visible: true });
		const frame = await this.page.frames().find(f => f.name() === name);
		const result = await frame.evaluate(selector => {
			let td = [...document.querySelectorAll(selector)];
			td.shift();
			const data = td.map(e => {
				let result = {};
				result.documentType = e.textContent.trim();
				return result;
			});
			return data;
		}, selector);
		this.data.extracted.documentType = result;
		return this;
	}

	async extractRecordedDate({ name, selector }) {
		await this.page.waitForSelector('[name="bodyframe"]');
		const bodyFrame = await this.page
			.frames()
			.find(f => f.name() === "bodyframe");
		await bodyFrame.waitForSelector('[name="resultFrame"]', { visible: true });
		const frame = await this.page.frames().find(f => f.name() === name);
		const result = await frame.evaluate(selector => {
			let td = [...document.querySelectorAll(selector)];
			td.shift();
			const data = td.map(e => {
				let result = {};
				result.recordedDate = e.textContent.trim();
				return result;
			});
			return data;
		}, selector);
		this.data.extracted.recordedDate = result;
		return this;
	}
	async extractBlockLot({ name, count }) {
		await this.page.waitForSelector('[name="bodyframe"]');
		let data = [];

		if (this.data.extracted.names.length === count) {
			count = count;
		} else {
			count = this.data.extracted.names.length;
		}
		for (let i = 0; i < count; i++) {
			debug(`Extracting block lot for index ${i}`);
			await this.page.waitForSelector('[name="bodyframe"]');
			const resultFrame = await this.page.frames().find(f => f.name() === name);
			const totalTable = await resultFrame.evaluate(
				i => loadRecord(documentRowInfo[i]),
				i
			);
			const bodyFrame = await this.page
				.frames()
				.find(f => f.name() === "bodyframe");
			await bodyFrame.waitForFunction(
				() =>
					document.querySelector("#progressContent").style.visibility !==
					"visible"
			);
			const frame2 = await this.page
				.frames()
				.find(f => f.name() === "docInfoFrame");
			await frame2.waitForSelector(
				"#data > table:nth-child(4) > tbody > tr:nth-child(1) > td:nth-child(3)",
				{ visible: true }
			);
			const blockLot = await frame2.evaluate(() => {
				const data = {};
				data.block = document
					.querySelector(
						"#data > table:nth-child(4) > tbody > tr:nth-child(3) > td:nth-child(3)"
					)
					.innerText.trim();
				data.lot = document
					.querySelector(
						"#data > table:nth-child(4) > tbody > tr:nth-child(4) > td:nth-child(3)"
					)
					.innerText.trim();
				data.book = document
					.querySelector(
						"#data > table:nth-child(4) > tbody > tr:nth-child(1) > td:nth-child(3)"
					)
					.innerText.trim();
				data.page = document
					.querySelector(
						"#data > table:nth-child(4) > tbody > tr:nth-child(2) > td:nth-child(3)"
					)
					.innerText.trim();
				data.secondary_name = document
					.querySelector(
						"#data > table:nth-child(16) > tbody > tr > td:nth-child(2) > table > tbody > tr > td"
					)
					.innerText.trim();
				return data;
			});
			data.push(blockLot);
		}

		this.data.extracted.data = data;
		return this;
	}
	async mergeResult() {
		const {
			data,
			names,
			instruments,
			municipalities,
			documentType,
			recordedDate
		} = this.data.extracted;
		const result = data.map((d, i) => ({
			name: names[i].name,
			block: d.block,
			lot: d.lot,
			book: d.book,
			page: d.page,
			secondary_name: d.secondary_name,
			instrument: instruments[i].instrument,
			municipality: municipalities[i].municipality,
			documentType: documentType[i].documentType,
			recordedDate: recordedDate[i].recordedDate
		}));
		this.data.extracted.result = result;
	}
}

async function runner({ displayRecord, documentType, dateRange }) {
	const scraper = new CustomScraper({});
	debug(`Starting at ${strftime("%Y-%m-%d %H:%M")}`);
	await scraper.init({ headless: true });
	async function runCommands() {
		const URL =
			"http://bergensearch.co.bergen.nj.us/countyweb/login.jsp?countyname=BergenRegistry";
		await scraper.runCommand({
			command: "navigate",
			message: "Navigate to Bergen Country",
			args: [URL]
		});
		await scraper.runCommand({
			command: "click",
			message: "Login as a guest",
			selector: ".basebold1"
		});
		await scraper.runCommand({
			command: "navigate",
			message: "Navigate to Bergen Country search page",
			args: ["http://bergensearch.co.bergen.nj.us/countyweb/disclaimer.do"]
		});
		await scraper.runCommand({
			command: "navigationClick",
			message: "Accept to login",
			selector: "#accept"
		});
		await scraper.runCommand({
			command: "setHiddenInput",
			message: "Set Display Record",
			name: "dynSearchFrame",
			selector: ".textbox-value",
			value: `${displayRecord}`
		});
		await scraper.runCommand({
			command: "clickFrame",
			message: "Set Recorded Date",
			name: "criteriaframe",
			selector:
				"#elemDateRange td:nth-child(4) input.textbox-text.validatebox-text"
		});
		await scraper.runCommand({
			command: "clickFrame",
			message: "Set Recorded Date Option",
			name: "criteriaframe",
			selector:
				dateRange === "Today"
					? "#_easyui_combobox_i2_1"
					: dateRange === "Yesterday"
					? "#_easyui_combobox_i2_2"
					: dateRange === "Week to Date"
					? "#_easyui_combobox_i2_3"
					: dateRange === "Month to Date"
					? "#_easyui_combobox_i2_4"
					: dateRange === "Year to Date"
					? "#_easyui_combobox_i2_5"
					: dateRange === "Last Week"
					? "#_easyui_combobox_i2_6"
					: dateRange === "Last Month"
					? "#_easyui_combobox_i2_8"
					: dateRange === "Last Year"
					? "#_easyui_combobox_i2_8"
					: "#_easyui_combobox_i2_8"
		});

		// await scraper.runCommand({
		// 	command: "removeTreeOptions",
		// 	message: "Remove All Tree Options",
		// 	name: "dynSearchFrame",
		// 	selector: ".tree-checkbox"
		// });

		// await scraper.runCommand({
		// 	command: "setTreeOptions",
		// 	message: "Set Tree Options",
		// 	name: "dynSearchFrame",
		// 	selector: ".tree-title",
		// 	documentType
		// });
		await scraper.runCommand({
			command: "clickFrame",
			message: "Click to search",
			name: "dynSearchFrame",
			selector: "#imgSearch"
		});
		await scraper.runCommand({
			command: "extractOtherName",
			message: "Extract Names",
			name: "resultListFrame",
			selector: ".datagrid-cell.datagrid-cell-c1-7"
		});
		if (scraper.data.extracted.error.length > 0) {
			return;
		} else {
			await scraper.runCommand({
				command: "extractInstrument",
				message: "Extract Instrument",
				name: "resultListFrame",
				selector: ".datagrid-cell.datagrid-cell-c1-3 span a"
			});
			await scraper.runCommand({
				command: "extractMunicipality",
				message: "Extract Municipality",
				name: "resultListFrame",
				selector: ".datagrid-cell.datagrid-cell-c1-10"
			});
			await scraper.runCommand({
				command: "extractDocumentType",
				message: "Extract DocumentType",
				name: "resultListFrame",
				selector: ".datagrid-cell.datagrid-cell-c1-8"
			});
			await scraper.runCommand({
				command: "extractRecordedDate",
				message: "Extract Recorded Date",
				name: "resultListFrame",
				selector: ".datagrid-cell.datagrid-cell-c1-9"
			});
			await scraper.runCommand({
				command: "extractBlockLot",
				message: "Extract Block an Lot",
				name: "resultListFrame",
				count: displayRecord
			});
			await scraper.runCommand({
				command: "mergeResult",
				message: "Merge Extracted Results"
			});
		}
	}
	try {
		// Run all the commands above
		await runCommands();
		// After completing close browser
		await scraper.closeBrowser();
	} catch (e) {
		await scraper.closeBrowser();
		scraper.data.extracted.error = e.message;
		debug(e);
		// return;
	}
	const { extracted } = scraper.data;
	return extracted;
}

module.exports = { step_1: runner };
