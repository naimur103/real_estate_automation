/**
STEP 3:

GOTO: https://www.realtytrac.com/login

LOGIN: username and password will be provided

SEARCH: Search with recorded Street, City state

RECORD: Estimated Value, Outstanding Loan Amount, Equity, Loan To Value (3. Property Loan Info)
 */
const puppeteer = require("puppeteer");
const strftime = require("strftime");
const debug = require("debug")(`scraper:`);
const Scraper = require("./common/Scraper");

class CustomScraper extends Scraper {
	constructor(props) {
		super(props);
		this.data = {
			extracted: {}
		};
		if (props.data) {
			this.data = Object.assign({}, this.data, props.data);
		}
	}
	async inputValue({ selector, value }) {
		await this.page.waitForSelector(selector);
		await this.page.evaluate(
			(selector, value) => (document.querySelector(selector).value = value),
			selector,
			value
		);
	}
	async extractPrices({ selector }) {
		await this.page.waitForSelector(selector);
		const prices = await this.page.evaluate(selector => {
			const priceDivs = document.querySelectorAll(selector);
			const estimated_value = priceDivs[0].textContent.trim();
			const outstanding_loan_amount = priceDivs[1].textContent.trim();
			const equity = priceDivs[2].textContent.trim();
			const loan_to_value = priceDivs[3].textContent.trim();
			return {
				estimated_value,
				outstanding_loan_amount,
				equity,
				loan_to_value
			};
		}, selector);
		this.data.extracted["ESTIMATED VALUE"] = prices.estimated_value;
		this.data.extracted["OUTSTANDING LOAN AMOUNT"] =
			prices.outstanding_loan_amount;
		this.data.extracted["EQUITY"] = prices.equity;
		this.data.extracted["LOAN TO VALUE"] = prices.loan_to_value;
		return this;
	}
	async extractStatus({ selector }) {
		const status = await this.page.evaluate(
			selector =>
				document.querySelector(selector) &&
				document.querySelector(selector).textContent.trim(),
			selector
		);
		this.data.extracted["STATUS"] = status;
		return this;
	}
	async extractPurchaseDate({ selector }) {
		const purchase_date = await this.page.evaluate(
			selector =>
				document.querySelector(selector) &&
				document.querySelector(selector).textContent.trim(),
			selector
		);
		this.data.extracted["PURCHASE DATE"] = purchase_date;
		return this;
	}
	async checkAddress({ selector }) {
		const exists = this.page.evaluate(selector => {
			const elem = document.querySelector(selector);
			return !!elem;
		}, selector);
		return exists;
	}
	async mergeResult({
		matched,
		prop_loc,
		district,
		owner,
		street,
		city_street,
		sale_date,
		price,
		nu,
		url_link,
		name,
		block,
		book,
		page,
		secondary_name,
		lot,
		instrument,
		municipality,
		recordedDate,
		documentType,
		priceDom
	}) {
		this.data.extracted.matched = matched;
		this.data.extracted["SECONDARY NAME"] = secondary_name;
		this.data.extracted["BOOK"] = book;
		this.data.extracted["PAGE"] = page;
		this.data.extracted["DOCUMENT TYPE"] = documentType;
		this.data.extracted["PROP LOC"] = prop_loc;
		this.data.extracted["DISTRICT"] = district;
		this.data.extracted["OWNER"] = owner;
		this.data.extracted["URL LINK"] = url_link;
		this.data.extracted["INSTRUMENT"] = instrument;
		this.data.extracted["RECORDED DATE"] = recordedDate;
		this.data.extracted["NAME"] = name;
		this.data.extracted["BLOCK"] = block;
		this.data.extracted["LOT"] = lot;
		this.data.extracted["MUNICIPALITY"] = municipality;
		this.data.extracted["LOT"] = lot;
		this.data.extracted["STREET"] = street;
		this.data.extracted["CITY STREET"] = city_street;
		this.data.extracted["SALE DATE"] = sale_date;
		this.data.extracted["PRICE"] = price;
		this.data.extracted["NU#"] = nu;
		if (!priceDom) {
			this.data.extracted["ESTIMATED VALUE"] = null;
			this.data.extracted["OUTSTANDING LOAN AMOUNT"] = null;
			this.data.extracted["EQUITY"] = null;
			this.data.extracted["LOAN TO VALUE"] = null;
		}
		return this;
	}
}

async function runner(
	{
		matched,
		prop_loc,
		district,
		owner,
		street,
		city_street,
		sale_date,
		price,
		nu,
		url_link,
		name,
		block,
		lot,
		book,
		page,
		instrument,
		municipality,
		recordedDate,
		documentType,
		secondary_name
	},
	{ username, password }
) {
	if (!matched) {
		return {
			NAME: name,
			BLOCK: block,
			LOT: lot,
			BOOK: book,
			PAGE: page,
			INSTRUMENT: instrument,
			MUNICIPALITY: municipality,
			"RECORDED DATE": recordedDate,
			"DOCUMENT TYPE": documentType,
			"SECONDARY NAME": secondary_name,
			matched
		};
	}
	const scraper = new CustomScraper({});
	debug(`Starting at ${strftime("%Y-%m-%d %H:%M")}`);
	await scraper.init({ headless: true });
	async function runCommands() {
		const URL = "https://www.realtytrac.com/";
		const LOGIN = "https://www.realtytrac.com/login";
		await scraper.runCommand({
			command: "navigate",
			message: "Navigate to realtytrac login page",
			args: [LOGIN]
		});
		await scraper.runCommand({
			command: "inputValue",
			message: "Enter username",
			selector: 'input[name="userName"]',
			value: `${username}`
		});
		await scraper.runCommand({
			command: "inputValue",
			message: "Enter password",
			selector: 'input[name="password"]',
			value: `${password}`
		});
		await scraper.runCommand({
			command: "click",
			message: "Submit Form",
			selector: "#btnLoginPageSubmit",
			navigation: true
		});
		await scraper.runCommand({
			command: "navigate",
			message: "Navigate to realtytrac home page",
			args: [URL]
		});
		await scraper.runCommand({
			command: "inputValue",
			message: "Enter search address",
			selector: 'input[name="txtSearch"]',
			value: `${street} ${city_street}`
		});
		await scraper.runCommand({
			command: "click",
			message: "Search address",
			selector: "#home_search_btn",
			navigation: true
		});
		const elem = await scraper.checkAddress({ selector: ".loan-area .price" });
		if (elem) {
			await scraper.runCommand({
				command: "extractPrices",
				message: "Extract Prices",
				selector: ".loan-area .price"
			});
			await scraper.runCommand({
				command: "extractStatus",
				message: "Extract Status",
				selector: ".foreclosure"
			});
			await scraper.runCommand({
				command: "extractPurchaseDate",
				message: "Extract Purchase Date",
				selector: "#occupancy-link > table > tbody > tr:nth-child(5) > td.col2"
			});
		}
		await scraper.runCommand({
			command: "mergeResult",
			message: "Merge all extracted data",
			priceDom: elem ? true : false,
			matched,
			prop_loc,
			district,
			owner,
			street,
			city_street,
			sale_date,
			price,
			nu,
			url_link,
			name,
			block,
			lot,
			instrument,
			municipality,
			recordedDate,
			documentType,
			book,
			page,
			secondary_name
		});
	}
	try {
		// Run all the commands above
		await runCommands();
		// After completing close browser
		await scraper.closeBrowser();
	} catch (e) {
		await scraper.closeBrowser();
		debug(e);
		return;
	}
	const { extracted } = scraper.data;
	return extracted;
}

module.exports = { step_3: runner };
