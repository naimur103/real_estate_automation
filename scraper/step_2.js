const puppeteer = require("puppeteer");
const strftime = require("strftime");
const debug = require("debug")(`scraper:`);
const Scraper = require("./common/Scraper");

class CustomScraper extends Scraper {
	constructor(props) {
		super(props);
		this.data = {
			extracted: {}
		};
		if (props.data) {
			this.data = Object.assign({}, this.data, props.data);
		}
	}
	async extractHrefs({ selector }) {
		const hrefs = await this.page.evaluate(selector => {
			let tds = [...document.querySelectorAll(selector)];
			const hrefs = tds.map(e => e.getAttribute("href"));
			return hrefs ? hrefs : [];
		}, selector);
		this.data.extracted.hrefs = hrefs;
		return this;
	}

	async navigateAndSearch({
		name,
		block,
		lot,
		book,
		page,
		secondary_name,
		instrument,
		municipality,
		documentType,
		recordedDate
	}) {
		const { hrefs } = this.data.extracted;
		const browser = await puppeteer.launch({
			headless: true,
			args: [
				"--disable-setuid-sandbox",
				"--disable-dev-shm-usage",
				"--no-sandbox",
				"--disable-features=site-per-process",
				"--no-first-run",
				"--ignore-certificate-errors"
			]
		});
		let data = {};
		if (hrefs.length > 0) {
			for (let i = 0; i < hrefs.length; i++) {
				try {
					const page = await browser.newPage();
					debug(`Navigating to name: ${name}, index: ${i}`);
					await page.goto(`http://tax1.co.monmouth.nj.us/cgi-bin/${hrefs[i]}`, {
						waitUntil: "networkidle2",
						timeout: 60 * 1000
					});
					const text = await page.evaluate(() =>
						document.querySelector("body").textContent.trim()
					);
					const splitedName = name.split(" ");
					if (text.includes(splitedName[0]) && text.includes(splitedName[1])) {
						const extractedData = await page.evaluate(() => {
							const prop_loc = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(1) > td:nth-child(4) > font"
								)
								.innerText.trim();
							const district = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(2) > td:nth-child(4) > font"
								)
								.innerText.trim();
							const owner = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(1) > td:nth-child(6) > font"
								)
								.innerText.trim();
							const street = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(2) > td:nth-child(6)"
								)
								.innerText.trim();
							const city_street = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(3) > td:nth-child(6)"
								)
								.innerText.trim();
							const sale_date = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(11) > td:nth-child(2)"
								)
								.innerText.trim();
							const price = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(11) > td:nth-child(6) font"
								)
								.innerText.trim();
							const nu = document
								.querySelector(
									"table:nth-child(3) tr:nth-child(11) > td:nth-child(6) > font:nth-child(3)"
								)
								.innerText.trim();
							return {
								prop_loc,
								district,
								owner,
								street,
								city_street,
								sale_date,
								price,
								nu
							};
						});
						data.prop_loc = extractedData.prop_loc;
						data.district = extractedData.district;
						data.owner = extractedData.owner;
						data.street = extractedData.street;
						data.city_street = extractedData.city_street;
						data.sale_date = extractedData.sale_date;
						data.price = extractedData.price;
						data.nu = extractedData.nu;
						data.url_link = `http://tax1.co.monmouth.nj.us/cgi-bin/${hrefs[i]}`;
						data.matched = true;
						break;
					} else {
						data.matched = false;
					}
					await page.close();
				} catch (error) {
					debug(error);
				}
			}
		} else {
			data.matched = false;
		}
		data.name = name;
		data.block = block;
		data.lot = lot;
		data.book = book;
		data.page = page;
		data.secondary_name = secondary_name;
		data.instrument = instrument;
		data.municipality = municipality;
		data.recordedDate = recordedDate;
		data.documentType = documentType;
		this.data.extracted.result = data;
		await browser.close();
		return this;
	}
}

async function runner({
	name,
	block,
	lot,
	book,
	page,
	secondary_name,
	instrument,
	municipality,
	documentType,
	recordedDate
}) {
	const scraper = new CustomScraper({});
	debug(`Starting at ${strftime("%Y-%m-%d %H:%M")}`);
	await scraper.init({ headless: true });
	async function runCommands() {
		const URL =
			"http://tax1.co.monmouth.nj.us/cgi-bin/prc6.cgi?&ms_user=monm&passwd=&district=0200&srch_type=1&out_type=2&adv=1";
		await scraper.runCommand({
			command: "navigate",
			message: "Navigate to monmouth",
			args: [URL]
		});

		await scraper.runCommand({
			command: "type",
			message: "Enter block",
			selector: '[name="block"]',
			text: `${block}`
		});

		await scraper.runCommand({
			command: "type",
			message: "Enter lot",
			selector: '[name="lot"]',
			text: `${lot}`
		});

		await scraper.runCommand({
			command: "click",
			message: "Submit",
			selector: '[type="Submit"]'
		});

		await scraper.runCommand({
			command: "extractHrefs",
			message: "Extract hrefs",
			selector: "tr a"
		});

		await scraper.runCommand({
			command: "navigateAndSearch",
			message: "Extract matched name info",
			name,
			block,
			lot,
			book,
			page,
			secondary_name,
			instrument,
			municipality,
			documentType,
			recordedDate
		});
	}
	try {
		// Run all the commands above
		await runCommands();
		// After completing close browser
		await scraper.closeBrowser();
	} catch (e) {
		await scraper.closeBrowser();
		debug(e);
		return;
	}
	const { result } = scraper.data.extracted;
	return result;
}

module.exports = { step_2: runner };
