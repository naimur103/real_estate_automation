window.delay = d => new Promise(r => setTimeout(r, d));

window.simulateMouseEvent = function simulateEvent(eventx, elem) {
	const event = new MouseEvent(eventx, {
		view: window,
		bubbles: true,
		cancelable: true
	});
	const cb = elem;
	return !cb.dispatchEvent(event);
};

window.simulateKeyboardEvent = function simulateEvent(eventx, elem) {
	const event = new KeyboardEvent(eventx, {
		view: window,
		bubbles: true,
		cancelable: true
	});
	const cb = elem;
	return !cb.dispatchEvent(event);
};

window.simulateEvent = window.simulateKeyboardEvent;

window.simulateType = function simulateType(selector, value) {
	const elem = document.querySelector(selector);
	elem.value = value;
	for (const event of [
		"click",
		"focus",
		"mousedown",
		"compositionend",
		"compositionstart"
	]) {
		simulateEvent(event, elem);
	}
};
