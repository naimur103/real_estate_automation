const utilSnippets = {
	/**
	 * Convert string of price to Number
	 * @param {String} str - quote price
	 */
	quoteToFloat(str) {
		return parseFloat(str.replace(/[^0-9.]/g, ""));
	},
	/**
	 * Convert millisecond to HH:MM:SS format.
	 * @param {Number} ms - millisecond
	 */
	msToHMS(ms) {
		// 1- Convert to seconds:
		let seconds = ms / 1000;
		// 2- Extract hours:
		const hours = parseInt(seconds / 3600, 16); // 3,600 seconds in 1 hour
		seconds %= 3600; // seconds remaining after extracting hours
		// 3- Extract minutes:
		const minutes = parseInt(seconds / 60, 16); // 60 seconds in 1 minute
		// 4- Keep only seconds not extracted to minutes:
		seconds %= 60;
		return `${hours}:${minutes}:${seconds}`;
	},
	/**
	 * Get current time in details
	 */
	getTime() {
		const date = new Date();
		const locale = "en-US";
		const result = date.toLocaleDateString(locale, {
			year: "numeric",
			month: "2-digit",
			day: "2-digit"
		});
		const today = result.split("/");
		const [month, day, year] = today;
		const data = {
			day,
			month,
			year,
			monthName: date.toLocaleString(locale, { month: "long" }),
			hour: date.getHours(),
			minute: date.getMinutes(),
			second: date.getSeconds()
		};
		return data;
	},

	cleanupKeyValue(args) {
		function decodeReplace(data) {
			return data.length && decodeURIComponent(data.replace(/\+/g, " "));
		}
		const { key = "", value = "" } = args;
		const newKey = decodeReplace(key);
		const newValue = decodeReplace(value);
		return Object.assign({}, args, { key: newKey, value: newValue });
	}
};

module.exports = utilSnippets;
