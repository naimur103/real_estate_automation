const puppeteer = require("puppeteer");
const normalizeUrl = require("normalize-url");
const fs = require("fs");
const debug = require("debug");
const utilSnippets = require("./util-snippets");

/**
 * creates a scraper instance
 */
class BrowserInstance {
	/**
	 * creates a Scraper
	 * @param {Object} props
	 * @param {Object|String} props.cookies
	 * @param {String} props.proxy
	 * @param {String} props.url
	 */
	constructor(props = {}) {
		this.props = props;
		return this;
	}

	/**
	 * Sets argument and creates a browser instance
	 */
	async init({ headless }) {
		await this.setCookies({});
		await this.setArgs();
		await this.launchBrowser(headless);
		await this.launchPage();
		return this;
	}

	/**
	 * Prepare Cookies for the browser instance
	 */
	setCookies() {
		try {
			switch (typeof this.props.cookies) {
				case "string":
					this.cookies = JSON.parse(this.props.cookies);
					break;
				case "object":
					this.cookies = this.props.cookies;
					break;
				default:
					this.cookies = [];
					break;
			}
		} catch (e) {
			debug("cannot parse cookies");
			this.cookies = [];
		}
		return this;
	}

	/**
	 * Set chrome specific arguments
	 */
	setArgs() {
		const args = [
			"--disable-setuid-sandbox",
			"--disable-dev-shm-usage",
			"--no-sandbox",
			"--disable-features=site-per-process",
			"--no-first-run",
			"--ignore-certificate-errors"
		];
		this.args = args;
		if (this.props.url) {
			this.props.url = normalizeUrl(this.props.url);
		}
		this.clickDelay = this.props.clickDelay || 1000;
		this.navigationArgs = { waitUntil: "networkidle0", timeout: 60 * 1000 };
		this.viewportArgs = { width: 800, height: 800 };
		return this;
	}

	/**
	 * Creates a browser with specific arguments
	 */
	async launchBrowser(headless) {
		this.browser = await puppeteer.launch({
			headless,
			args: this.args
		});
		return this;
	}

	/**
	 * Creates a page and processes various events
	 */
	async launchPage() {
		this.page = await this.browser.newPage();
		this.page.setDefaultNavigationTimeout(60000); // NOTE: hardcoded right now for bug test
		await this.page.evaluateOnNewDocument(
			fs.readFileSync(`${__dirname}/preload.js`, "utf8")
		);

		if (this.cookies) {
			await this.page.setCookie(...this.cookies);
		}
		await this.page.setViewport(this.viewportArgs);
		return this;
	}

	/**
	 * Closes page and browser
	 */
	async closeBrowser() {
		try {
			await this.page.close();
			await this.browser.close();
		} catch (e) {
			debug("Cannot close instance");
			throw e;
		}
	}
}

class PageController extends BrowserInstance {
	/**
	 * Navigate to Url
	 * @param {String} url - url to navigate
	 */
	async navigate(url = this.props.url) {
		this.response = await this.page.goto(url, this.navigationArgs);
		return this;
	}

	/**
	 * Type on input field
	 * @param {String} selector - Target input
	 * @param {String} text - value to input on the field
	 */
	async type({ selector, text }) {
		try {
			await this.page.waitForSelector(selector, { visible: true });
			await this.page.focus(selector);
			await this.page.type(selector, text);
			await this.page.evaluate(
				dom => simulateEvent("focus", document.querySelector(dom)),
				selector
			);
		} catch (e) {
			debug(`Cannot Type on ${selector}`, e);
		}
	}

	/**
	 * Click on target on the dom
	 * @param {String} selector - Target click
	 * @param {Boolean} visible - Visibility of that selector
	 */
	async click({ selector, visible = true, navigation }) {
		try {
			await this.page.waitForSelector(selector, { visible });
			await this.page.focus(selector);
			await this.page.evaluate(
				dom => document.querySelector(dom).click(),
				selector
			);
			await this.page.waitFor(this.clickDelay);
			if (navigation) {
				await this.waitForNavigation();
			}
		} catch (e) {
			debug(`Cannot Click :: ${selector}`);
		}
	}

	/**
	 * Select option of input type "select"
	 * @param {String} selector - selector for input type "select"
	 * @param {String} value - value to select
	 */
	async select({ selector, value }) {
		try {
			await this.page.waitForSelector(selector);
			await this.page.focus(selector);
			await this.page.select(selector, value);
		} catch (e) {
			debug(`Cannot select: ${selector}`);
		}
	}

	/**
	 * Get text of a selector
	 * @param {String} selector - target selector
	 */
	async getText(selector) {
		await this.page.waitForSelector(selector);
		return this.page.evaluate(
			dom => document.querySelector(dom).textContent,
			selector
		);
	}

	/**
	 * Run the class functions
	 * @param {Object} args - arguments
	 * @param {Function} args.command
	 * @param {String} args.message
	 * @param {String} args.customArgs
	 */
	async runCommand(args) {
		const { command, message, args: customArgs } = args;
		debug(`scraper:`)(`${command}:: ${message}`);

		if (typeof customArgs === "string") {
			const resp = await this[command](customArgs);
			return resp;
		}

		if (Array.isArray(customArgs)) {
			const resp = await this[command](...customArgs);
			return resp;
		}

		const resp = await this[command](args);
		await this.page.waitFor(1000);
		return resp;
	}

	async waitFor({ waitForArgs }) {
		await this.page.waitFor(...waitForArgs);
	}

	/**
	 * Wait for target selector to visible on dom
	 */
	async waitForSelector({ selector }) {
		await this.page.waitForSelector(selector);
	}

	/**
	 * Wait for navigation
	 * @param {Object} param0 - arguments
	 * @param {String} param0.args - event of waitUntil. exp: 'load', 'domcontentloaded'
	 */
	async waitForNavigation(args) {
		if (args) {
			await this.page.waitForNavigation({ waitUntil: args });
		}
		await this.page.waitForNavigation({ waitUntil: "networkidle0" });
	}

	/**
	 * Check if the element is exists on dom
	 */
	async elemExists(args) {
		const newArgs = utilSnippets.cleanupKeyValue(args);
		return this.page.evaluate(arg => {
			const elem = document.querySelector(arg.key || arg.selector);
			return !!elem;
		}, newArgs);
	}

	/**
	 * Click on target if element is exists otherwise go to next
	 */
	async navigationClick({ selector, visible = true }) {
		try {
			await this.page.waitForSelector(selector, { visible });
			await this.page.focus(selector);
			await this.page.click(selector);
			await this.page.waitFor(10000);
		} catch (e) {
			debug(`Cannot Click :: ${selector}`);
		}
	}
}

module.exports = PageController;
