const PQueue = require("p-queue");
const queue = new PQueue({ concurrency: 2 });
const { step_1 } = require("./step_1");
const { step_2 } = require("./step_2");
const { step_3 } = require("./step_3");

async function appRunner(inputData) {
	const step_1_data = await step_1(inputData);
	// let step_1_data = [
	// 	{
	// 		name: "SAMPIETRO DONALD J",
	// 		block: "504",
	// 		lot: "2",
	// 		book: "02819",
	// 		page: "1130",
	// 		secondary_name: "DONALD J SAMPIETRO",
	// 		instrument: "V 02819 1130",
	// 		municipality: "NORTHVALE",
	// 		documentType: "TAX WAIVER",
	// 		recordedDate: "12/26/2017"
	// 	}
	// ];
	let step_2_data = [];
	let step_3_data = [];
	if (step_1_data.error.length > 0) {
		return { error: step_1_data.error };
	} else {
		for (let i = 0; i < step_1_data.length; i++) {
			queue
				.add(() => step_2(step_1_data[i]))
				.then(data => step_2_data.push(data));
		}

		return queue.onIdle().then(() => {
			for (let i = 0; i < step_2_data.length; i++) {
				queue
					.add(() => step_3(step_2_data[i], inputData))
					.then(data => step_3_data.push(data));
			}
			return queue.onIdle().then(() => step_3_data);
		});
	}
}

// const inputData = {
// 	displayRecord: 10,
// 	documentType: ["tax waiver"],
// 	username: "asdasfsdf@aol.com",
// 	password: "password"
// };
// appRunner(inputData).then(data => debug([].concat.apply([], data)));
module.exports = appRunner;
